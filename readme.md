# Sieve of Erathostenes

Task: Use the Sieve of Eratosthenes  to write a C program that does the following: 
 1. The program gets a value N (which is limited to 1005) from the user.
 2. The program generates the Nth prime number. 

## Geting started
```
$ gcc sievetest.c -o sieve -lm
$ ./sieve manual
$ ./sieve automatic primes.txt
```

### Design insights!

documentation.txt contains some fluff.
optimized versions of sieve and benchmarks? Soon™

### Tests!

This implementation comes with
  - Driver program for manual testing
  - Automatic test suite

### Other OS? 
Should be portable. Tested on 64bit Debian

### Feedback
Great!
