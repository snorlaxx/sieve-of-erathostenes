#include <math.h> //for logarithm
#define MAX_N 1005

int sieve(int n)
{
  //first 6 primes are held in memory
  int firstprimes[7] = { 0, 2, 3, 5, 7, 11, 13 };
  int primes_found = 0;
  if (n > 0 && n < 7) //est. arraysize does not hold true for these
  {
    return firstprimes[n];
  }
  else if(n < 1 || n > MAX_N) //negative numbers return error, could abs here
  {
   return -1;
  }

  /* approximate size of array: (log(log(N))N + log(N)) * N > prime(N)
  * this holds true for N >= 6
  * https://en.wikipedia.org/wiki/Prime_number_theorem#Approximations_for_the_nth_prime_number
  */
  long array_size = n * (log(n) + log(log(n))) +1;
  long i;
  int * numberlist;
  /*
  * calloc will initialize all fields to 0
  */
  numberlist = (int*)calloc(array_size, sizeof(int));
  for(i = 2; i < array_size; i++)
  {
    //fields containing 0 are prime (not crossed out)
    if(numberlist[i] == 0)
    {
      long j = i;
      while(j < array_size)
      {
        numberlist[j] = -1;
        j = j + i;
      }
      primes_found += 1;
      if(primes_found >= n)
      {
        free(numberlist);
        return i;
      }
    }
    else
    {
      ;//this field has been crossed out
    }
  }
  free(numberlist);
  printf("error calculating %dth prime. Terminating\n", n);
  exit(1);
}
