#include <stdlib.h>
#include <stdio.h> //for file io and read/write to std
#include <string.h> //for string comparisons
#include <inttypes.h> //for str to int conversion
#include "sieve.c"
#ifndef MAX_N
#define MAX_N 1005
#endif
static int correct_primes[MAX_N];
static int calculated_primes[MAX_N];
static int extra_test_cases[] = {-1006, -1005, -1, 0, 1, 1006};
static int extra_test_results[] = {-1 , -1, -1, -1, 2, -1};
/*
 * reads a list of primes from disk
 * invokes sieve(N) for each number in the list
 * IMPORTANT: adjust MAX_N for larger lists
*/
static void init_prime_list(const char* filename)
{
  //Get filehandle
  FILE *primefile = fopen(filename, "r");
  if (primefile == NULL)
  {
    fprintf(stderr, "Error opening %s\n",filename);
    exit(1);
  }
  //read the list of primes and copy into memory
  //correct_primes[0]=0; //there is no 0th prime
  int i = 0;
  int current_prime;
  while(fscanf(primefile, "%d,", &current_prime) != EOF)
  {
    correct_primes[i] = current_prime;
    i += 1;
  }
  //close file
  if(fclose(primefile) != 0)
  {
    printf("error closing %s\n", filename);    
  }
}

/*
 *
*/
static void sievetest_automatic() 
{
  //test all primes in range
  int i;
  int tests_passed=0;
  int extra_tests_passed=0;
  printf("testing all primes in range...\n");
  for (i = 1; i <= MAX_N; i++)
  {
    calculated_primes[i] = sieve(i);
    if(calculated_primes[i] == correct_primes[i])
    {
      tests_passed += 1;
    }
    else
    {
      printf("fail: %d != %d\n",calculated_primes[i],correct_primes[i]);
    }
  }

  //test corner cases
  for (i = 0; i < (sizeof(extra_test_cases)/sizeof(extra_test_cases[0]));i++) 
    {
    int res = sieve(extra_test_cases[i]);
    if (res == extra_test_results[i])
    {
      extra_tests_passed += 1;
    }
    else
    {
      printf("test failed: sieve(%d) = %d\n",i , res);
    }
  }
  printf("tests complete.\n");
  printf("passed %d out of %d prime tests\n", tests_passed, MAX_N);
  printf("passed %d out of %ld extra tests\n", 
    extra_tests_passed, sizeof(extra_test_cases)/sizeof(extra_test_cases[0]) );
}

void usage()
{
  printf("usage:\n sieve --manual\n sieve --automatic filename");
}

int main (int argc, char *argv[])
{
  char userinput[10];
  //automatic test mode? print only results
  if(argc == 3 && strcmp(argv[1], "--automatic") == 0)
  {
    printf("Initializing automatic tests\n");
    init_prime_list(argv[2]);
    printf("Prime table loaded\n");
    sievetest_automatic();
  }

  //Manual test mode? (Read from stdin and print to stdout)
  else if(argc == 2 && strcmp(argv[1], "--manual") == 0)
  {
    printf("Which prime do you want to calculate? (1-1005)\n"
      "or type exit to quit\n");
    while(strncmp(userinput , "exit", 4) != 0)
    {
      fgets(userinput, sizeof(userinput) , stdin); //blocking
      int input_number = strtoimax(userinput, NULL, 10);
      printf("%dth prime is: %d\n", input_number, sieve(input_number));
    }
  }
  //wrong parameters: print manual and exit
  else
  {
    usage();
    exit(1);
  }
  //all tests done or user typed exit
  return 0; 
}
